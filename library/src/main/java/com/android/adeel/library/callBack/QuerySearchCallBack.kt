package com.android.adeel.library.callBack

import com.android.adeel.library.model.Result

interface QuerySearchCallBack {

    public fun getQuerySearchResult(response: MutableMap<String, ArrayList<Result>>)
}