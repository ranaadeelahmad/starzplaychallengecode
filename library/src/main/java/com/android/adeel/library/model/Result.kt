package com.android.adeel.library.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class Result : Serializable {
    @SerializedName("poster_path")
    @Expose
    var posterPath: Any? = null
    @SerializedName("popularity")
    @Expose
    var popularity: Double? = null
    @SerializedName("vote_count")
    @Expose
    var voteCount: Double? = null
    @SerializedName("video")
    @Expose
    var video: Boolean? = null
    @SerializedName("media_type")
    @Expose
    var mediaType: String? = null
    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("adult")
    @Expose
    var adult: Boolean? = null
    @SerializedName("backdrop_path")
    @Expose
    var backdropPath: Any? = null
    @SerializedName("original_language")
    @Expose
    var originalLanguage: String? = null
    @SerializedName("original_title")
    @Expose
    var originalTitle: String? = null
    @SerializedName("genre_ids")
    @Expose
    var genreIds: List<Int>? = null
    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("vote_average")
    @Expose
    var voteAverage: Double? = null
    @SerializedName("overview")
    @Expose
    var overview: String? = null
    @SerializedName("release_date")
    @Expose
    var releaseDate: String? = null
    @SerializedName("known_for_department")
    @Expose
    var knownForDepartment: String? = null
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("known_for")
    @Expose
    var knownFor: List<Result>? = null
    @SerializedName("profile_path")
    @Expose
    var profilePath: Any? = null
    @SerializedName("gender")
    @Expose
    var gender: Int? = null
    @SerializedName("original_name")
    @Expose
    var originalName: String? = null
    @SerializedName("first_air_date")
    @Expose
    var firstAirDate: String? = null
    @SerializedName("origin_country")
    @Expose
    var originCountry: List<String>? = null

}