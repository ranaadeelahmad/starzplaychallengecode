package com.android.adeel.library.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class SearchResult {
    @SerializedName("results")
    @Expose
    private var results: List<Result?>? = null

    fun getResults(): List<Result?>? {
        return results
    }

    fun setResults(results: List<Result?>?) {
        this.results = results
    }
}