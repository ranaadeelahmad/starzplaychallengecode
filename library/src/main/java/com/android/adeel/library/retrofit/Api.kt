package com.android.adeel.library.retrofit

import com.android.adeel.library.model.SearchResult
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {

    @GET("search/multi")
    fun getSearchResult(
        @Query("api_key") apiKey: String,
        @Query("query") query: String
    ): Call<SearchResult>
}