package com.android.adeel.library

import android.content.Context
import android.widget.Toast
import com.android.adeel.library.callBack.QuerySearchCallBack
import com.android.adeel.library.model.Result
import com.android.adeel.library.model.SearchResult
import com.android.adeel.library.retrofit.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class QuerySearch(val context: Context) {

    fun searchTheResult(word: String, querySearchCallBack: QuerySearchCallBack) {
        RetrofitClient.instance.getSearchResult(Companion.apiKey, word)
            .enqueue(object : Callback<SearchResult> {
                override fun onFailure(call: Call<SearchResult>, t: Throwable) {
                    Toast.makeText(context, "failure: Api fail", Toast.LENGTH_SHORT).show()
                }

                override fun onResponse(
                    call: Call<SearchResult>,
                    response: Response<SearchResult>
                ) {
                    SortArrayInCoursalOder(response.body()?.getResults()).let {
                        querySearchCallBack.getQuerySearchResult(
                            it
                        )
                    }

                }
            })

    }

    fun SortArrayInCoursalOder(searchResult: List<Result?>?): MutableMap<String, ArrayList<Result>> {
        var sectionList: MutableMap<String, ArrayList<Result>> =
            mutableMapOf<String, ArrayList<Result>>()
        if (searchResult.isNullOrEmpty()) {
            return sectionList
        }

        for (results: Result? in searchResult) {
            if (sectionList.containsKey(results?.mediaType)) {
                val pointArray: ArrayList<Result> = sectionList.get(results?.mediaType)!!
                if (results!!.knownFor.isNullOrEmpty()) {
                    pointArray.add(results)
                } else {
                    pointArray.addAll(results.knownFor!!)
                }
                sectionList.put(results.mediaType!!, pointArray)
            } else {
                if (results!!.knownFor.isNullOrEmpty()) {
                    val pointArray: ArrayList<Result> = ArrayList(results)
                    sectionList.put(results.mediaType!!, pointArray)
                } else {
                    val pointArray: ArrayList<Result> = ArrayList(results.knownFor!!)
                    sectionList.put(results.mediaType!!, pointArray)
                }
            }
        }
        return sectionList
    }

    private fun ArrayList(results: Result?): java.util.ArrayList<Result> {
        val list = ArrayList<Result>()
        list.add(results!!)
        return list
    }

    companion object {
        const val apiKey = "3d0cda4466f269e793e9283f6ce0b75e"
    }
}

