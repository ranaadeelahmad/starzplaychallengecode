package com.android.adeel.starzplaychallengecode.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.android.adeel.library.model.Result
import com.android.adeel.starzplaychallengecode.R
import com.android.adeel.starzplaychallengecode.util.Constants
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activtiy_detail_screen.*

class DetailScreenActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activtiy_detail_screen)

        val result = intent.getSerializableExtra(Constants.EXTRA_MESSAGE) as? Result

        if (result?.title == null)
            movieTitle.setText(result?.name)
        else
            movieTitle.setText(result.title)

        voteAverage.setText(result?.voteAverage.toString())
        description.setText(result?.overview)
        Glide.with(this)
            .load(Constants.IMAGE_INTIAL_URL + result?.backdropPath)
            .into(back_drop_image)

        if (result?.mediaType.equals("movie") || result?.mediaType.equals("tv"))
            playBt.visibility = View.VISIBLE
        else
            playBt.visibility = View.GONE

        playBt.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, PlayerScreenActivity::class.java).apply {}
            startActivity(intent)
        })

    }
}