package com.android.adeel.starzplaychallengecode.util

object Constants {
    const val IMAGE_INTIAL_URL = "https://image.tmdb.org/t/p/w200"
    const val EXTRA_MESSAGE = "EXTRA_MESSAGE"
    const val MEDIA_URL =
        "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4"
}