package com.android.adeel.starzplaychallengecode.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.SearchView.OnQueryTextListener
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.adeel.library.QuerySearch
import com.android.adeel.library.callBack.QuerySearchCallBack
import com.android.adeel.library.model.Result
import com.android.adeel.starzplaychallengecode.R
import com.android.adeel.starzplaychallengecode.adapter.SectionAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(),
    QuerySearchCallBack {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        listofItem.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        searchView.setOnQueryTextListener(object : OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                // task HERE
                progressbar.visibility = View.VISIBLE
                QuerySearch(context = this@MainActivity).searchTheResult(query, this@MainActivity)
                return false
            }

        })


    }

    override fun getQuerySearchResult(response: MutableMap<String, ArrayList<Result>>) {

        progressbar.visibility = View.GONE
        if (response.isEmpty()) {
            Toast.makeText(this, "No item's found!", Toast.LENGTH_LONG).show()
            return
        }
        Log.e("Response", response.keys.toString())
        listofItem.adapter =
            SectionAdapter(
                response,
                this
            )

    }
}
