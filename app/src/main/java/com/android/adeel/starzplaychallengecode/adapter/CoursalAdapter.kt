package com.android.adeel.starzplaychallengecode.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.adeel.library.model.Result
import com.android.adeel.starzplaychallengecode.util.Constants
import com.android.adeel.starzplaychallengecode.activity.DetailScreenActivity
import com.android.adeel.starzplaychallengecode.R
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.coursal_item.view.*

class CoursalAdapter(
    val item: ArrayList<Result>,
    val context: Context
) : RecyclerView.Adapter<ViewHolderCoursalAdapter>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderCoursalAdapter {
        return ViewHolderCoursalAdapter(
            LayoutInflater.from(context).inflate(
                R.layout.coursal_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return item.size
    }

    override fun onBindViewHolder(holder: ViewHolderCoursalAdapter, position: Int) {
        var title: String? = item.get(position).title
        if (title.isNullOrBlank())
            title = item.get(position).name
        holder.name?.text = title
        Glide.with(context)
            .load(Constants.IMAGE_INTIAL_URL + item.get(position).posterPath)
            .into(holder.poster)
        holder.itemView.setOnClickListener(View.OnClickListener {
            val intent = Intent(context, DetailScreenActivity::class.java).apply {
                putExtra(Constants.EXTRA_MESSAGE, item.get(position))
            }
            context.startActivity(intent)
        })

    }


}

class ViewHolderCoursalAdapter(view: View) : RecyclerView.ViewHolder(view) {
    val poster = view.movie_image
    val name = view.movie_name
}