package com.android.adeel.starzplaychallengecode.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.adeel.library.model.Result
import com.android.adeel.starzplaychallengecode.R
import kotlinx.android.synthetic.main.coursal_section_item.view.*

class SectionAdapter(
    val item: MutableMap<String, ArrayList<Result>>,
    val context: Context
) : RecyclerView.Adapter<ViewHolderSectionAdapter>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderSectionAdapter {
        return ViewHolderSectionAdapter(
            LayoutInflater.from(context).inflate(
                R.layout.coursal_section_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return item.keys.size
    }

    override fun onBindViewHolder(holder: ViewHolderSectionAdapter, position: Int) {
        val key: String = item.keys.elementAt(position)
        holder.title?.text = key
        holder.coursalRecyclview.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        holder.coursalRecyclview.adapter =
            CoursalAdapter(
                item.getValue(key),
                context
            )

    }


}

class ViewHolderSectionAdapter(view: View) : RecyclerView.ViewHolder(view) {
    val title = view.title
    val coursalRecyclview = view.listofcoursal
}